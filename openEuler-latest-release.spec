Name:           openEuler-latest-release
Summary:        System information like kernelversion, openeulerversion, gccversion, openjdkversion and compile time
License:        GPL-1.0-or-later
Group:          System/Config
Version:        1.0
Release:        1
Source1:        isopackage.sdf
Source2:        isopackage_arm64.sdf
BuildRoot:      %{_tmppath}/%{name}-%{version}
BuildRequires:  gcc kernel-devel java-1.8.0-openjdk

%description
The rpm contains system information, like kernelversion, eulerversion and compile time so on.

%setup -q

%install
mkdir -p %{buildroot}/etc
%ifarch aarch64
install %{_sourcedir}/isopackage_arm64.sdf %{buildroot}/etc/openEuler-latest
%else
install %{_sourcedir}/isopackage.sdf %{buildroot}/etc/openEuler-latest
%endif

%pre

%post
if [[ `grep "openeulerversion" /etc/openEuler-latest | cut -d '_' -f2` =~ 2\.2\.RC.* ]]; then
    if [ $1 = 1 ]; then
	if [ -e /etc/openEulerLinux.conf ];then
	    mv /etc/openEulerLinux.conf /etc/.openEulerLinux.conf
	fi
    else
	rm -f /etc/.openEulerLinux.conf
    fi
fi

%preun

%postun
if [ -e /etc/.openEulerLinux.conf ]; then
mv /etc/.openEulerLinux.conf /etc/openEulerLinux.conf
fi

%files
%config /etc/openEuler-latest
%attr(0444, root, root) /etc/openEuler-latest

%clean
rm -rf $RPM_BUILD_ROOT/*
rm -rf %{_tmppath}/%{name}-%{version}
rm -rf $RPM_BUILD_DIR/%{name}-%{version}

%changelog
* Thu Jul 28 2022 Chenyx <chenyixiong3@huawei.com> - 1.0-1
- License compliance rectification